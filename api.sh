apt install tmux -y
git clone https://gitlab.com/madwgn/sc-api /etc/api
pip3 install -U pip
pip3 install fastapi uvicorn pytz aiofiles
cat > /etc/systemd/system/wgnapi.service << END
[Unit]
Description=Simple api - @madewgn
After=network.target

[Service]
WorkingDirectory=/etc/api
ExecStart=python3 main.py
Restart=always

[Install]
WantedBy=multi-user.target
END


pip3 install -U pip
pip3 install fastapi uvicorn pytz aiofiles


systemctl start wgnapi
systemctl enable wgnapi
systemctl restart wgnapi




# restart api
cd /etc/
wget https://gitlab.com/madwgn/apex/-/raw/master/res-api.sh
cat > /etc/systemd/system/res-api.service << END
[Unit]
Description=auto restart api - @madewgn
After=network.target

[Service]
WorkingDirectory=/etc/
ExecStart=bash res-api.sh
Restart=always

[Install]
WantedBy=multi-user.target
END



systemctl start res-api
systemctl enable res-api
systemctl restart res-api




