#!/bin/bash
# ==========================================
# Color
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT='\033[0;37m'
clear
# ==========================================
echo -e "${ORANGE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "                MEMBER SSH                   "
echo -e "${ORANGE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo "USERNAME          EXP DATE          "
echo -e "${ORANGE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
while read expired
do
    AKUN="$(echo $expired | cut -d: -f1)"
    ID="$(echo $expired | grep -v nobody | cut -d: -f3)"
    exp="$(chage -l $AKUN | grep "Account expires" | awk -F": " '{print $2}')"
    status="$(passwd -S $AKUN | awk '{print $2}' )"
    if [[ $ID -ge 1000 ]]; then
        if [[ "$status" = "L" ]]; then
            printf "%-17s %2s %-17s %2s \n" "$AKUN" "$exp     "
        else
            printf "%-17s %2s %-17s %2s \n" "$AKUN" "$exp     "
        fi
    fi
done < /etc/passwd
echo -e "${ORANGE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo -e "                DELETE SSH                   "
echo -e "${ORANGE}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\033[0m"
echo ""

# Delete users whose username starts with "trial"
echo "Deleting users with usernames starting with 'trial':"
while read expired
do
    AKUN="$(echo $expired | cut -d: -f1)"
    ID="$(echo $expired | grep -v nobody | cut -d: -f3)"
    if [[ $ID -ge 1000 && $AKUN == trial* ]]; then
        if getent passwd $AKUN > /dev/null 2>&1; then
            userdel $AKUN > /dev/null 2>&1
            echo -e "User $AKUN Berhasil Di Hapus."
        else
            echo -e "Failure: User $AKUN Tidak Di Temukan."
        fi
    fi
done < /etc/passwd

echo -e "\nProses penghapusan selesai."
menu