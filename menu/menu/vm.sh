#!/bin/bash

read -p "username: " user
read -p "uuid: " uuid
read -p "exp: " exp

user_EXISTS=$(grep -w "$user" /etc/xray/vmess.json | wc -l)

if [[ $user_EXISTS -eq 1 ]]; then
    echo "Username atau Nama sudah digunakan!"
else
    if ! grep -q "$user" /etc/xray/vmess.json; then
        sed -i '/#vmess$/a\### '"$user $exp"'\
        },{"id": "'""$uuid""'","alterId": '"0"',"email": "'""$user""'"' /etc/xray/vmess.json
        sed -i '/#vmessgrpc$/a\### '"$user $exp"'\
        },{"id": "'""$uuid""'","alterId": '"0"',"email": "'""$user""'"' /etc/xray/vmess.json
        systemctl restart vmess
        echo 'Berhasil menambahkan pengguna dan me-restart layanan'
    else
        echo "Pengguna sudah ada dalam file"
    fi
fi