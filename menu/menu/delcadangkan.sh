#!/bin/bash
# ==========================================
# Color
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHT='\033[0;37m'

clear
echo -e "${GREEN}Memulai proses penghapusan backup...${NC}"

# Menghentikan service backup
systemctl stop trojan-backup
systemctl stop vmess-backup
systemctl stop vless-backup

# Menonaktifkan service backup
systemctl disable trojan-backup
systemctl disable vmess-backup
systemctl disable vless-backup

# Menghapus file service
rm -f /etc/systemd/system/trojan-backup.service
rm -f /etc/systemd/system/vmess-backup.service
rm -f /etc/systemd/system/vless-backup.service

# Menghapus folder backup
rm -rf /etc/backup1

# Reload daemon
systemctl daemon-reload

echo -e "${GREEN}Backup berhasil dihapus!${NC}"
