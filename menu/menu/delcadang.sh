#!/bin/bash

TOKEN="1785564306:AAGdI-cwa6HEkoJSL2I6o5KxqQGRZc-HmfA"  # Ganti dengan token bot Telegram kamu
CHAT_ID="1207375389"           # Ganti dengan chat ID atau group ID kamu

send_telegram() {
    local message=$1
    curl -s -X POST "https://api.telegram.org/bot$TOKEN/sendMessage" \
        -d chat_id="$CHAT_ID" \
        -d text="$message"
}

user="cadangan"
uuid="1a1eaff0-7a77-457f-af68-de024c088a0a"

# ----- Remove Trojan User -----
exp=$(grep -wE "^#! $user" "/etc/xray/trojan.json" | cut -d ' ' -f 3 | sort | uniq)
sed -i "/^#! $user $exp$/,/\"email\": \"$user\"/d" /etc/xray/trojan.json
sed -i "/### $user $exp/,/^},{/d" /etc/trojan/.trojan.db
rm -rf /etc/trojan/$user
rm -rf /etc/kyt/limit/trojan/ip/$user
systemctl restart trojan > /dev/null 2>&1
send_telegram "**CADANGAN** : Akun Trojan dengan username $user telah dihapus."

# ----- Remove VMess User -----
exp=$(grep -wE "^### $user" "/etc/xray/vmess.json" | cut -d ' ' -f 3 | sort | uniq)
sed -i "/^### $user $exp$/,/\"email\": \"$user\"/d" /etc/xray/vmess.json
sed -i "/^### $user $exp/,/^},{/d" /etc/vmess/.vmess.db
rm -rf /etc/vmess/$user
rm -rf /etc/kyt/limit/vmess/ip/$user
systemctl restart vmess > /dev/null 2>&1
send_telegram "**CADANGAN** : Akun VMess dengan username $user telah dihapus."

# ----- Remove VLess User -----
exp=$(grep -wE "^#& $user" "/etc/xray/vless.json" | cut -d ' ' -f 3 | sort | uniq)
sed -i "/^#& $user $exp/,/^},{/d" /etc/xray/vless.json
sed -i "/^#& $user $exp/,/^},{/d" /etc/vless/.vless.db
rm -rf /etc/vless/$user
rm -rf /etc/kyt/limit/vless/ip/$user
systemctl restart vless > /dev/null 2>&1
send_telegram "**CADANGAN** : Akun VLess dengan username $user telah dihapus."

# ----- Update cadang.txt -----
echo "Off" > /etc/cadang.txt
send_telegram "Status cadang.txt telah diubah menjadi Off."

